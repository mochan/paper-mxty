from PyPDF2 import PdfReader
from langchain.text_splitter import RecursiveCharacterTextSplitter


def pdf_to_text(pdf_path, llm):
    # Read the text from the pdf file
    pdf = PdfReader(pdf_path)
    text = ""
    pages = []
    for page in pdf.pages:
        page = page.extract_text()  # .encode('ascii', 'ignore').decode()
        text += page
        pages.append(page)

    chunk_sizes = [10000, 7500, 5000, 2500, 1000]

    # If splitting the text into chunks fails, chunks is returned as an empty list
    chunks = []

    # Split the text into chunks
    # If splitting the text into chunks fails, try again with a smaller chunk size
    for chunk_size in chunk_sizes:
        print('Using chunk size: ', chunk_size)

        text_splitter = RecursiveCharacterTextSplitter(
            chunk_size=chunk_size,
            chunk_overlap=500,
            length_function=len
        )
        chunks = text_splitter.split_text(text)

        # Check that no chunk has more than 4000 tokens
        chunk_too_large = False
        for chunk in chunks:
            if llm.get_num_tokens(chunk) > 4000:
                chunk_too_large = True
                break

        if not chunk_too_large:
            break

    return pages, text, chunks
