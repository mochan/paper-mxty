import os

# Main folder where the user data is stored
user_data = 'user_data'

# Folders inside user data that we store extra data
chats_folder_name = 'chats'
chats_folder = os.path.join(user_data, chats_folder_name)
criticisms_folder_name = 'criticisms'
criticisms_folder = os.path.join(user_data, criticisms_folder_name)
data_folder_name = 'data'
data_folder = os.path.join(user_data, data_folder_name)
embeddings_folder_name = 'embeddings'
embeddings_folder = os.path.join(user_data, embeddings_folder_name)
mcqs_folder_name = 'mcqs'
mcqs_folder = os.path.join(user_data, mcqs_folder_name)
summaries_folder_name = 'summaries'
summaries_folder = os.path.join(user_data, summaries_folder_name)

# List of all the folders that we expect to have inside the user_data folder
sub_folder_names = [chats_folder_name, criticisms_folder_name, data_folder_name, embeddings_folder_name,
                    mcqs_folder_name, summaries_folder_name]
sub_folders = [chats_folder, criticisms_folder, data_folder, embeddings_folder, mcqs_folder, summaries_folder]
