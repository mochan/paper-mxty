import json
import os
import pickle
import random
import streamlit as st
from langchain import OpenAI, PromptTemplate, FAISS
from langchain.chains.question_answering import load_qa_chain
from langchain.chains.summarize import load_summarize_chain
from langchain.chat_models import ChatOpenAI
from langchain.callbacks import get_openai_callback
from langchain.embeddings import OpenAIEmbeddings
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.docstore.document import Document

from config import *
from constants import *
from templates import *
from pdf_to_text import pdf_to_text
from utils import check_and_create_folders


# Given the template, generate a prompt and ask llm. Check if we have cached data first
# If not, ask llm and cache the data
# If so, retrieve the data from the cache
def ask_llm(llm, template, chunk_all_summaries, section_name):
    cache_filename = os.path.join(data_folder, section_name + ".md")
    if os.path.exists(cache_filename):
        with open(cache_filename, "r") as f:
            return f.read()
    else:
        prompt = PromptTemplate(input_variables=['text'], template=template)
        prompt_text = prompt.format(text=chunk_all_summaries)
        llm_output = llm(prompt_text)
        with open(cache_filename, "w") as f:
            f.write(llm_output)
        return llm_output


# Get the embeddings for the paper in a vector store so that it can be queried
def get_embeddings(embeddings_filename, text):
    embeddings_filepath = os.path.join(embeddings_folder, embeddings_filename)
    if os.path.exists(embeddings_filepath):
        # Load embeddings from file
        with open(embeddings_filepath, "rb") as f:
            vector_store = pickle.load(f)
    else:
        # Create an embeddings object
        embeddings = OpenAIEmbeddings()

        # Split the text into chunks
        text_splitter = RecursiveCharacterTextSplitter(
            chunk_size=1000,
            chunk_overlap=200,
            length_function=len
        )
        chunks = text_splitter.split_text(text)

        # Create a vector store object
        vector_store = FAISS.from_texts(chunks, embedding=embeddings)

        # Store the embeddings in a file
        with open(embeddings_filepath, "wb") as f:
            pickle.dump(vector_store, f)

    return vector_store


# Generate a new quiz and save to file giving the context and the questions previously asked
def generate_new_quiz(context, llm_creative, llm, q1, q2, q3, q4, q5):
    # Generate a new quiz
    prompt = PromptTemplate(input_variables=['q1', 'q2', 'q3', 'q4', 'q5', 'json_format', 'text'],
                            template=multiple_choice_template)
    prompt_text = prompt.format(text=context, json_format=json_format, q1=q1, q2=q2, q3=q3, q4=q4, q5=q5)
    llm_output = llm_creative(prompt_text)

    # Write the llm output to screen to console to see what it looks like
    print(llm_output)

    try:
        json_data = json.loads(llm_output)
    except json.decoder.JSONDecodeError:
        prompt_fix_json = PromptTemplate(input_variables=['text', 'json_format'],
                                         template=fix_json_template)
        prompt_text = prompt_fix_json.format(text=llm_output, json_format=json_format)
        valid_json = llm(prompt_text)
        print(valid_json)
        json_data = json.loads(valid_json)

    # Parse the output to questions, choices and answers
    # json_data = json.loads(llm_output)
    question = json_data["Question"]
    question_choices = json_data["Options"]
    answer_index = json_data["Answer"]

    # If answer_index is a string, convert it to an integer
    if isinstance(answer_index, str):
        answer_index = int(answer_index)

    return question, question_choices, answer_index


# Generate a new quiz and save to file giving the context and choosing a random sub-context of size context_size
def generate_new_quiz_reduce_context(context, llm_creative, llm, context_size):
    # Create a new context of size context_size
    context_len = len(context)
    if context_len <= context_size:
        sub_context = context
    else:
        start_index = random.randint(0, context_len - context_size)
        while start_index > 0 and context[start_index] != " ":  # Find s word break
            start_index -= 1
        end_index = start_index + context_size
        while end_index < context_len and context[end_index] != " ":  # Find a word break
            end_index += 1
        sub_context = context[start_index:end_index]

    # Generate a new quiz
    prompt = PromptTemplate(input_variables=['json_format', 'text'], template=multiple_choice_template_no_history)
    prompt_text = prompt.format(text=sub_context, json_format=json_format)
    print(prompt_text)
    llm_output = llm_creative(prompt_text)

    # Write the llm output to screen to console to see what it looks like
    print(llm_output)

    try:
        json_data = json.loads(llm_output)
    except json.decoder.JSONDecodeError:
        prompt_fix_json = PromptTemplate(input_variables=['text', 'json_format'],
                                         template=fix_json_template)
        prompt_text = prompt_fix_json.format(text=llm_output, json_format=json_format)
        valid_json = llm(prompt_text)
        print(valid_json)
        json_data = json.loads(valid_json)

    # Parse the output to questions, choices and answers
    # json_data = json.loads(llm_output)
    question = json_data["Question"]
    question_choices = json_data["Options"]

    # If answer_index is a string, convert it to an integer
    answer_index = json_data["Answer"]
    if isinstance(answer_index, str):
        answer_index = int(answer_index)

    # If answer_index is a string, convert it to an integer
    if isinstance(answer_index, str):
        answer_index = question_choices.index(answer_index)

    return question, question_choices, answer_index


# Write the new question to file
def write_new_quiz(questions, choices, answers, mcq_filepath, question, question_choices, answer_index):
    questions.insert(0, question)
    choices.insert(0, question_choices)
    answers.insert(0, answer_index)

    # Write the questions to file
    with open(mcq_filepath, "wb") as f:
        f.write(json.dumps([questions, choices, answers], indent=2).encode("utf-8"))


def get_quizzes(file_prefix):
    # Check if the file in quizzes exists
    quizzes_filepath = os.path.join(chats_folder, file_prefix + "_quizzes.json")
    if os.path.exists(quizzes_filepath):
        with open(quizzes_filepath, "rb") as f:
            quizzes = json.load(f)
    else:
        quizzes = []
    return quizzes


# Get the options for the quiz and multiple choice questions
def get_options_for_quiz(num_pages):
    """
    Get the options for the quiz.
    It adds the pages as options for the quiz from the number of pages in the pdf
    :param num_pages: Number of pages in the pdf
    :return: Options to display the user
    """
    question_types = ["Main Summary", "Summaries", "Paper (Random Section)"]
    for i in range(num_pages):
        question_types.append("Page " + str(i + 1))
    return question_types


# Given the option selected, give the text context that we will be using
def get_context_from_options(question_types, question_type, summary_data, chunk_all_summaries, pages, text):
    question_type_index = question_types.index(question_type)

    if question_type_index == 0:
        # Get the main summary
        context = summary_data["output_text"]
    elif question_type_index == 1:
        # Get all the summaries
        context = chunk_all_summaries
    elif question_type_index == 2:
        # Get a random section from the paper
        # Get a random number between 2500 and 5000 for the length of the section
        random_section_length = random.randint(2500, 5000)
        random_section_start = random.randint(0, len(text) - random_section_length)
        random_section = text[random_section_start:random_section_start + random_section_length]
        context = random_section
    else:
        # Get the page number and return the page as context
        page_number = question_type_index - 2
        context = pages[page_number - 1]

    return context


def main():
    # Title of the page
    st.header("Paper MXTY")

    # area to upload a pdf file
    uploaded_file = st.file_uploader("Upload a PDF file", type=["pdf"])

    # Streamlit session management stuff
    if "new_mcq_added" not in st.session_state:
        st.session_state.new_mcq_added = False

    # LLM that we will be using
    llm = OpenAI(temperature=0, model_name='gpt-3.5-turbo')
    llm_creative = OpenAI(temperature=0.7, model_name='gpt-3.5-turbo')

    # if a file is uploaded, create a reader object
    if uploaded_file is not None:
        # Check that all the folders have been created that we want to store data in
        check_and_create_folders()

        pages, text, chunks = pdf_to_text(uploaded_file, llm)

        docs = [Document(page_content=chunk) for chunk in chunks]

        # Keep track of the API cost associated with the operations
        with get_openai_callback() as cb:
            # File prefix
            file_prefix = os.path.splitext(uploaded_file.name)[0].replace(" ", "_")

            # Get the embeddings for the paper in a vector store so that it can be queried
            embeddings_filename = file_prefix + ".pkl"
            vector_store = get_embeddings(embeddings_filename, text)

            # Summary file name
            summary_filename = file_prefix + ".pkl"
            summary_filepath = os.path.join(summaries_folder, summary_filename)

            # Check if we have a cached summary for this paper. If so, load it. If not, generate it
            if os.path.exists(summary_filepath):
                # Load embeddings from file
                with open(summary_filepath, "rb") as f:
                    summary_data = pickle.load(f)
            else:
                chain = load_summarize_chain(ChatOpenAI(temperature=0, model_name='gpt-3.5-turbo'),
                                             chain_type='map_reduce',
                                             return_intermediate_steps=True)
                summary_data = chain({"input_documents": docs}, return_only_outputs=True)
                # Write to pickle file
                with open(summary_filepath, "wb") as f:
                    pickle.dump(summary_data, f)

            # Calculate an appropriate title and subtitle for the paper
            chunk_summaries = summary_data["intermediate_steps"]
            chunk_all_summaries = ""
            for chunk_summary in chunk_summaries:
                chunk_all_summaries += chunk_summary

            # Get the title and subtitle for this paper
            st.write(ask_llm(llm, template_title_subtitle, chunk_all_summaries, file_prefix + ".title_subtitle"))

            # Get the summary for this paper
            st.header("Summary")
            st.write(summary_data["output_text"])

            # Get the prerequisites for this paper
            st.write('## Prerequisites')
            st.write(ask_llm(llm, prerequisites_template, chunk_all_summaries, file_prefix + ".prerequisites"))

            # Get the inverted triangle for this paper
            st.write('## Background and Motivation')
            st.write(ask_llm(llm, background_motivation_template, chunk_all_summaries,
                             file_prefix + ".background_motivation"))

            # Get the details of the method used in the paper
            st.write("## Method Details")
            st.write(ask_llm(llm, method_details_template, chunk_all_summaries, file_prefix + ".method_details"))

            # Get the results of the paper
            st.header("Results")
            st.write(ask_llm(llm, results_template, chunk_all_summaries, file_prefix + ".results"))

            # tokens = llm.get_num_tokens(prompt_text)
            # st.write(tokens)

            # Get the conclusion of the paper
            st.write('## Conclusion')
            st.write(ask_llm(llm, conclusion_template, chunk_all_summaries, file_prefix + ".conclusion"))

            # Get the future work of the paper
            st.write('## Future Work')
            st.write(ask_llm(llm, future_work_template, chunk_all_summaries, file_prefix + ".future_work"))

            st.markdown("---")

            # Create the criticisms for the paper
            st.write('## Shortcomings')

            criticism_filename = file_prefix + ".pkl"
            criticism_filepath = os.path.join(criticisms_folder, summary_filename)

            if os.path.exists(criticism_filepath):
                # Load the criticisms of the paper from file
                with open(criticism_filepath, "rb") as f:
                    criticism_data = pickle.load(f)
            else:
                # Create the criticisms of the paper
                map_prompt = PromptTemplate(template=criticism_map_template, input_variables=["text"])
                combine_prompt = PromptTemplate(template=criticism_combine_template, input_variables=["text"])
                chain = load_summarize_chain(ChatOpenAI(temperature=0.9, model_name='gpt-3.5-turbo'),
                                             chain_type='map_reduce',
                                             return_intermediate_steps=True, map_prompt=map_prompt,
                                             combine_prompt=combine_prompt)
                criticism_data = chain({"input_documents": docs}, return_only_outputs=True)
                # Write to pickle file
                with open(criticism_filepath, "wb") as f:
                    pickle.dump(criticism_data, f)

            # Display the criticisms of the paper
            st.write(criticism_data["output_text"])

            # Write what future work could fix the shortcomings
            st.write('## Future Work from Shortcomings')
            st.write(ask_llm(llm_creative, shortcomings_work_template, criticism_data["output_text"],
                             file_prefix + ".future_work_shortcomings"))

            st.markdown("---")

            # Create a chat box for the user to chat with the bot about the pdf
            st.write('## Chat')

            # Check if we have the file chat.pkl
            chat_filepath = os.path.join(chats_folder, file_prefix + ".json")
            if os.path.exists(chat_filepath):
                # Load embeddings from file
                with open(chat_filepath, "rb") as f:
                    messages = json.load(f)
            else:
                messages = [
                    ["Hello, I am a bot that can answer questions about the PDF you uploaded.", False],
                ]

            # Create a text input box for the user to query the pdf
            query = st.text_input("Ask a question about the PDF")
            if query:
                # Omit repeat questions
                repeat_question = False
                for msg in messages:
                    if msg[0] == query:
                        repeat_question = True
                        break

                if not repeat_question:
                    messages.insert(0, [query, True])

                    # Get the answer to the question
                    query_docs = vector_store.similarity_search(query=query, k=3)
                    chain = load_qa_chain(llm, chain_type='stuff')
                    response = chain.run(input_documents=query_docs, question=query)
                    messages.insert(0, [response, False])

            for msg in messages:
                if msg[1]:
                    st.write("<div style='text-align: right'>" + msg[0] + " 👤</div><br>",
                             unsafe_allow_html=True)
                else:
                    st.write(
                        "<div style='border:1px solid rgb(234, 234, 234);border-radius: 15px'>🤖 ",
                        msg[0],
                        "</div>", unsafe_allow_html=True)
            # Write the messages to file
            with open(chat_filepath, "wb") as f:
                f.write(json.dumps(messages, indent=2).encode("utf-8"))

            st.markdown("---")

            # Check if we have the file mcq.json for multiple choice questions
            mcq_filepath = os.path.join(mcqs_folder, file_prefix + ".json")
            if not os.path.exists(mcq_filepath):
                # Create a new multiple choice question to get started and save the question to file
                question, question_choices, answer_index = generate_new_quiz(chunk_all_summaries, llm_creative, llm, "",
                                                                             "", "", "", "")
                write_new_quiz([], [], [], mcq_filepath, question, question_choices, answer_index)

            # Load multiple choice questions from file
            with open(mcq_filepath, "rb") as f:
                questions, choices, answers = json.load(f)

            st.write('## Multiple-Choice Questions')

            st.write('### Generate a new multiple choice question.')
            question_types = get_options_for_quiz(len(pages))
            question_type = st.selectbox("Please select a question type:", question_types)
            if st.button('Generate new question'):
                # Generate the question
                prompt = PromptTemplate(input_variables=['q1', 'q2', 'q3', 'q4', 'q5', 'json_format', 'text'],
                                        template=multiple_choice_template)

                # Get the index of the question type
                context = get_context_from_options(question_types, question_type, summary_data, chunk_all_summaries,
                                                   pages, text)

                # Get the question
                question1 = ''
                question2 = ''
                question3 = ''
                question4 = ''
                question5 = ''
                if len(questions) > 0:
                    question1 = questions[0]
                if len(questions) > 1:
                    question2 = questions[1]
                if len(questions) > 2:
                    question3 = questions[2]
                if len(questions) > 3:
                    question4 = questions[3]
                if len(questions) > 4:
                    question5 = questions[4]
                # question, question_choices, answer_index = generate_new_quiz(context, llm_creative, llm, question1,
                #                                                              question2, question3, question4, question5)
                question, question_choices, answer_index = generate_new_quiz_reduce_context(context, llm_creative, llm,
                                                                                            1250)

                questions.insert(0, question)
                choices.insert(0, question_choices)
                answers.insert(0, answer_index)

                # Write the questions to file
                with open(mcq_filepath, "wb") as f:
                    f.write(json.dumps([questions, choices, answers], indent=2).encode("utf-8"))

            # Generate a new question from a custom prompt
            st.write('#### Generate a new question from a custom prompt')
            custom_prompt = st.text_input("Enter a custom prompt")
            if custom_prompt:
                query_docs = vector_store.similarity_search(query=custom_prompt, k=3)
                mc_llm = OpenAI(temperature=0.8, model_name='gpt-3.5-turbo')
                chain = load_qa_chain(mc_llm, chain_type='stuff')
                mcq_question = 'Create a multiple choice question about ' + custom_prompt + ' and give the correct answer' + \
                               'Reply in the following JSON format: { Question: Question, Options: [Option 0, Option 1, Option 2, Option 3], Answer: Answer as index }'
                llm_output = chain.run(input_documents=query_docs, question=mcq_question)
                st.write(llm_output)

                # Check that the llm output is valid json
                try:
                    json_data = json.loads(llm_output)
                except json.decoder.JSONDecodeError:
                    prompt_fix_json = PromptTemplate(input_variables=['text', 'json_format'],
                                                     template=fix_json_template)
                    prompt_text = prompt_fix_json.format(text=llm_output, json_format=json_format)
                    valid_json = llm(prompt_text)
                    st.write(valid_json)
                    json_data = json.loads(valid_json)

                # Parse the output to questions, choices and answers
                # json_data = json.loads(llm_output)
                question = json_data["Question"]
                question_choices = json_data["Options"]
                answer_index = json_data["Answer"]

                # If answer_index is a string, convert it to an integer
                if isinstance(answer_index, str):
                    answer_index = question_choices.index(answer_index)

                questions.insert(0, question)
                choices.insert(0, question_choices)
                answers.insert(0, answer_index)

                # Write the questions to file
                with open(mcq_filepath, "wb") as f:
                    f.write(json.dumps([questions, choices, answers], indent=2).encode("utf-8"))

            st.markdown("---")

            # Create a select-box for the question
            question = st.selectbox("Please select a question:", questions)

            # Find the index of the question
            index = questions.index(question)

            # Write the question
            st.write('#### Question: ', questions[index])

            # Display the choices related to the selected question
            choice = st.radio("Your answer:", choices[index])

            if st.button('Check my answer'):
                # Compare the choice with the correct answer
                if choice == choices[index][answers[index]]:
                    st.success('Congratulations! Your answer is correct.')
                else:
                    st.error('Sorry, your answer is incorrect. Please try again.')

            # Quiz and add questions and evaluate the answers
            st.write('---')
            st.write('### Quiz')

            quizzes = get_quizzes(file_prefix)

            question_types = get_options_for_quiz(len(pages))
            question_type = st.selectbox("Please select where to ask a question from:", question_types)
            if st.button('Quiz me'):
                st.session_state.quiz = True

                # Get the index of the question type
                context = get_context_from_options(question_types, question_type, summary_data, chunk_all_summaries,
                                                   pages, text)
                st.session_state.context = context

                # Ask a question
                quiz_prompt = PromptTemplate(input_variables=['text', 'q1', 'q2', 'q3', 'q4', 'q5'],
                                             template=quiz_template)
                question1 = ''
                question2 = ''
                question3 = ''
                question4 = ''
                question5 = ''
                if len(quizzes) > 0:
                    question1 = quizzes[0][0]
                if len(quizzes) > 1:
                    question2 = quizzes[1][0]
                if len(quizzes) > 2:
                    question3 = quizzes[2][0]
                if len(quizzes) > 3:
                    question4 = quizzes[3][0]
                if len(quizzes) > 4:
                    question5 = quizzes[4][0]
                prompt_text = quiz_prompt.format(text=context, q1=question1, q2=question2, q3=question3, q4=question4,
                                                 q5=question5)
                print(prompt_text)
                question = llm_creative(prompt_text)
                st.write(question)

                st.session_state.question = question

            if 'quiz' in st.session_state:
                if st.session_state.quiz:

                    # Create the area for the answer
                    answer = st.text_area("Enter your answer here")

                    # Evaluate the answer and display the result
                    if answer:
                        # Evaluate the question
                        eval_prompt = PromptTemplate(input_variables=['question', 'answer', 'text'],
                                                     template=eval_template)
                        prompt_text = eval_prompt.format(question=st.session_state.question, answer=answer,
                                                         text=st.session_state.context)
                        evaluation = llm(prompt_text)

                        # Add it to the file
                        quizzes.insert(0, [st.session_state.question, answer, evaluation])
                        quizzes_filepath = os.path.join(chats_folder, file_prefix + "_quizzes.json")
                        with open(quizzes_filepath, "wb") as f:
                            f.write(json.dumps(quizzes, indent=2).encode("utf-8"))

                        st.session_state.quiz = False
                        st.session_state.question = None
                        st.session_state.context = None

                        st.button('Clear')

            st.write('#### Past Quizzes')

            # Check if the file in quizzes exists
            quizzes = get_quizzes(file_prefix)
            for quiz in quizzes:
                st.write("<div style='border:1px solid rgb(234, 234, 234);border-radius: 15px'>",
                         unsafe_allow_html=True)
                st.write('❓ ', quiz[0])
                st.write("<div style='text-align: right'>💬 ", quiz[1], "</div>", unsafe_allow_html=True)
                st.write('📊 ', quiz[2])
                st.write('</div>', unsafe_allow_html=True)

            # Horizontal line
            st.markdown("---")
            st.write("Chat API Cost: ", cb.total_cost)
            print(cb)


if __name__ == "__main__":
    os.environ["OPENAI_API_KEY"] = OPENAI_API_KEY
    main()
