import os

from constants import *


# Create all the needed folders to store the user data from the pdfs
def check_and_create_folders():
    # Create the user_data folder if it does not exist
    if not os.path.exists(user_data):
        os.mkdir(user_data)

    # Create the sub folders if they do not exist
    for sub_folder in sub_folders:
        if not os.path.exists(sub_folder):
            os.mkdir(sub_folder)
