# Paper-Mxty

Paper Mxty is a tool to help read pdf and pdf papers.

Using an LLM it creates:

- summaries
- few page presentation bullet points
- PDF chat tool to ask questions about the paper
- Get LLM to ask you about the paper (multiple choice or questions)

Note that using this will incur fees in the OpenAI API. The estimated cost for each step is listed at the bottom.

## Installation and Usage

Since this uses the OpenAI API, you will need to have an OpenAI API key.

1. Setup the PipEnv environment. `Pipfile` has all the required packages that needs to be installed. Use `pipenv install` to install all the packages.
1. Rename `config.example.py` to `config.py` and fill in the OpenAI API key in the file.
1. To run the app, run `streamlit run paper_mxty.py` in the terminal. 