"""
This file contains all the templates that we will be using in the bot.
"""

json_format = "{ Question: Question, Options: [Option0, Option1, Option2, Option3], " \
              "Answer: Answer as index (0,1,2 or 3) }"

# Templates that we will be using
multiple_choice_template = """Given the following context, create a multiple choice question and the correct answer; do not ask a question related to these questions - ["{q1}", "{q2}", "{q3}", "{q4}", "{q5}"] and reply in the following JSON format: {json_format}

Context: "{text}"
"""

multiple_choice_template_no_history = """
Given the following context, create a multiple choice question and the correct answer; 
reply in the following JSON format: {json_format}

Context: "{text}"
"""

fix_json_template = """Rewrite the text in the following valid JSON format: {json_format}

text: "{text}"
"""

# Title and subtitle for this paper
template_title_subtitle = """
Given the following summary of a paper, what would be a good title and subtitle for the paper?
Please reply as markdown in the following format:
# Title of the paper
## Subtitle of the paper

"{text}
"
"""

# Prerequisites for this paper
prerequisites_template = """
Given the following summary of a paper, what are the prerequisites for this paper?
Please reply as markdown in the following format:
- Prerequisite 1
- Prerequisite 2
- Prerequisite 3
- Prerequisite 4
- Prerequisite 5
- Prerequisite 6
- Prerequisite 7

"{text}
"
"""

# The inverted triangle for this paper for the background, motivation to the actual problem and solution
background_motivation_template = """
Given the following summary of a paper, give the background and motivation of the paper, 
the problem that the paper is  solving, and the solution that the paper is proposing and the effectiveness 
of the solution. 
Please reply as markdown in the following format:
- Background 
- Motivation
- General Problem
- Proposed Solution
- Effectiveness of Solution

"{text}
"
"""

# Details of the method used in the paper
method_details_template = """
Given the following summary of a paper, give the details of the proposed method to the problem it is solving. 
Please reply as markdown in the following format:
- Proposal 1
- Proposal 2
- Proposal 3

"{text}
"
"""

# Results of the paper
results_template = """
Given the following summary of a paper, give the details of the results and how the their method solves the 
problem. Please reply as markdown in the following format:
- Result 1
- Result 2
- Result 3

"{text}
"
"""

# Conclusion of the paper
conclusion_template = """
Given the following summary of a paper, give the conclusion of the paper.
Please reply as markdown in the following format:
- Conclusion 1
- Conclusion 2
- Conclusion 3

"{text}
"
"""

# Template for future work from the summaries
future_work_template = """
Given the following summary of a paper, give the future work of the paper.
Please reply as markdown in the following format:
- Future Work 1
- Future Work 2
- Future Work 3

"{text}
"
"""

# Templates that we will be using for writing criticism of the paper
criticism_map_template = """Write a concise high-level scientific shortcomings of the following section of a paper. 
Limit shortcomings to only ideas of the paper and not technical writing, missing description, style or content.

Context: "{text}"
"""

criticism_combine_template = """Give a list of high-level scientific shortcomings of the paper from  
the following collection of shortcomings of different parts of the paper. 
Limit shortcomings to only high-level scientific ideas of the paper.
Please reply as markdown in the following format:
- Criticism 1
- Criticism 2
- Criticism 3
- Criticism 4

Context: "{text}"
"""

shortcomings_work_template = """
Given the following summary of a paper, give the future work of the paper.
Please reply as markdown in the following format:
- Future Work 1
- Future Work 2
- Future Work 3

"{text}
"
"""

# Templates that we will be using for asking a quiz question
quiz_template = """
The questions "{q1}", "{q2}", "{q3}", "{q4}", "{q5}" have already been asked.
Given the following text, ask a new question from it to evaluate my understanding of it and do not ask a question similar to that has already been asked.

{text}
"""

# Templates that we will be using for evaluating a quiz question's answer from the user
eval_template = """
The answer to the question "{question}" is "{answer}" and is form the text below.
If correct, say it is correct and describe why it is correct. 
If not correct, say it is incorrect and say describe why it is incorrect.

{text}
"""
